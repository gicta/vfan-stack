const fastify = require('fastify');
const path = require('path');
const app = fastify({
    logger: true
})
const api_port = 3333; //todo magic number
const app_port = 3000; //todo magic number

const memberQuery = require('./dbQuery/memberQuery');


//const users = [];
//app.use(express.static(path.join(__dirname, '../app/build')));
//todo we don't have webpack, so there is no build yet
//app.set('etag', false);

//API Routes
app.post('/api/authenticateMember', async (request, reply) => {
  console.log('HTTP GET api/authenticateMember'); //todo real log
  //debug: console.log(`request body is ${JSON.stringify(request.body)}`)
  let memberLogin = request.body.memberLogin;
  let memberPassword = request.body.memberPassword;
  //debug: console.log(`${memberLogin}|${memberPassword}`);

  try {
    var result = await memberQuery.authenticateMember(memberLogin, memberPassword);
    reply.type('application/json').code(200);
    reply.send(result);         
  }
  catch (e) {
    console.log(`EXCEPTION: ${e}`);
    reply.type('application/json').code(500);
    reply.send('{"error": "It was... the yeti"}');         
  }

});

app.get('/api/members', async (request, reply) => {
    console.log('HTTP GET api/members'); //todo real log

    try {
      var members = await memberQuery.getAllMembers;
      reply.type('application/json').code(200);
      reply.send(members);         
    }
    catch (e) {
      console.log(`EXCEPTION: ${e}`);
      reply.type('application/json').code(500);
      reply.send('{"error": "It was... the yeti"}');         
    }
});

app.get('/api/user/:id', (req, res) =>{
    const id = req.params['id'].toString();
    var feedback = '';    
    if(id==='1') {
        console.log(`HTTP GET api/user/${id} equals 1`);
        res.statusCode = 200;
        feedback = "user 1";
    }
    else {
        console.log(`HTTP GET api/user/${id} not equals 1`);
        res.statusCode = 200; 
        feedback = "user not found";
    }
    console.log(`feedback ${feedback}`);
    res.json(feedback);
});

// app.post('/api/user', (req, res) => {
//     const user = req.body.user; //todo fact check
//     console.log(`HTTP POST api/user [${user}]`);
//     users.push(user); //todo fact check
//     res.json("OK"); //todo or not
//     res.end();
// });

app.get('/', (req,res) => {
    console.log("HTTP GET /");
    console.log("Endpoint not supported. Prolly a 404. Hmm 404."); //todo 404
    res.statusCode = 404;
    res.json("404");
})

console.log(`Server listening on port:${api_port}`);
app.listen(api_port, '0.0.0.0');
