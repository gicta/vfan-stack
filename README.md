# vena-stack
Update: Is now technically VFAN (Vue Fastify Airtable Node)...
Original: Messing around with Vue Express Node & Airtable (could also back with MySQL, but let's have some fun yo)

## Running
git clone repo \
api folder has api project \
app folder has Vue app project \
npm install needs to be run in api and app folders separately \
need two terminal windows \
terminal 1: npm run prod to start api server on port 3333 \
terminal 2: npm run serve to start app server on port 8080 \
visit localhost:8080 to see sample vue app \
visit localhost:3333/api/members to see sample api call (currently not working, lol)\

## Troubleshooting
Node/Express - code has crappy (none really) error handling, so if something goes wrong, things are just going to hang there forever.  Two options: \
- comment things out until something works, then start re-adding code slowly
- add some error handling and dump more output to console.log

## Secrets
Secrets are a thing, and there are multiple ways to do it, but I think the schluessel package looks pretty elegant, so going to take that for a spin. \
From their docs: \
- from your project, use <code>npx schluessel new</code> to create vault (credentials) file and associated key (credentials.key)
- the above command defaults to DEV environment, use <code>NODE_ENV=bleebloo npx schluessel new</code> to target another environment
- use <code>NODE_ENV=bleebloo npx schluessel edit</code> to edit the file - this will decrypt it and encrypt when your IDE closes
- schluessel also adds the credential file to .gitignore
- file is JSON format, up to you, but this project is using \
<code>{
  "_description": "Put your credentials here...",
  "airTable": {
    "apiKey": "your.api.key",
    "BaseID": "your.base.id"
  }
}</code>
- keep a copy of your keys outside your project, and in a safe place.  Lost=screwed, shared=screwed
- I forked the repo and made some mods: https://github.com/gicta/schluessel which the owner denied.  He wrote me a very long explanation that made complete sense, but I will keep the fork in case I come up with better ideas in the future.

## Research

First off I wanna run NodeJS in a Docker container. So I hit [this url](https://code.visualstudio.com/docs/containers/quickstart-node)

Was having issues with async/await and promises in Express. After about 4 hours of research (Aka, not a whole lot of time), I eventually stumbled into [this article]https://thecodebarbarian.com/building-rest-apis-with-async-await-and-fastify.html \
 \
I ported the project to Fastify and async/await started working as expected. Alrighty then, this is now VFNA stack... maybe NAVF is the way to go there. \
 \
Had to tweak their proxy code ('http://' being most notable change) \
 \
In addition I am using some content from [this post](https://medium.com/bb-tutorials-and-thoughts/how-to-develop-and-build-vue-js-app-with-nodejs-bd86feec1a20) \
 \
(npm install nodemon --save-dev, npm install body-parser, for example) \
 \
Decided to use Airtable's support library (ferp surprise) \
<code>npm install airtable</code> \







