import './assets/style.css'; //this totally works - awesome
import { createApp } from 'vue';
import App from './App.vue'
import router from './router/index';

createApp(App).use(router).mount('#app');
