/* handles interactions with member API */
import {baseController} from './baseController';

export class memberController extends baseController {

  constructor(apiBaseUrl) {
    super(apiBaseUrl);
  }

  //authenticate
  async authenticateMember(memberLogin, memberPassword) {

    console.log(`calling authenticateMember API ${this.apiBaseUrl} with body: ${JSON.stringify({memberLogin, memberPassword})}`);
    const payload = JSON.stringify({memberLogin, memberPassword});
    const response = await this.apiPost('/authenticateMember', payload);
    return response;
  }
}
