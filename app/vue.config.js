//proxy calls to the API on 3333
module.exports = {
    devServer: {
        proxy: {
            '^/api': {
                target: 'http://localhost:3333', //todo magic number
                changeOrigin: true,
                secure: false,
                pathRewrite: {'^/api': '/api'},
                logLevel: 'debug'
            }
        }
    }
}